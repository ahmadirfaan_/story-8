from django import forms
from .models import Status

class StatusForm(forms.ModelForm):
	status = forms.CharField(label="", widget=forms.TextInput(attrs={
		"id":"forms"
	}))
	class Meta:
		model = Status
		fields = ('status',)