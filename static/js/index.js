  setInterval(function(){  
    test()
    }, 3000);

function test(){
  $('#loading').remove();
  $('#hide').css('display', 'block')
}


var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  });
}   


$(document).ready(function() {
  var colorOrig=$("body").css('background-color');
  var colorGreetOrig=$(".greetings1").css('background-color');
  $("#buttonTheme1").hover(
  function() {
      //mouse over
      $("body").css('background', 'orange');
      $(".greetings1").css('background-color', 'lightcoral')
  }, function() {
      //mouse out
      $("body").css('background', colorOrig);
      $(".greetings1").css('background-color', colorGreetOrig)
  });
  $("#buttonTheme2").hover(
    function() {
        //mouse over
        $("body").css('background', 'white');
        $(".greetings1").css('background-color', 'rgb(150, 150, 150)')

    }, function() {
        //mouse out
        $("body").css('background', colorOrig);
        $(".greetings1").css('background-color', colorGreetOrig)
  });
  $("#buttonTheme1").click(function(){
    colorGreetOrig = "lightcoral";
    colorOrig = "orange";
    $("body").css("background-color", "orange");
    $(".greetings1").css('background-color', 'lightcoral');
  });
  $("#buttonTheme2").click(function(){
    colorGreetOrig = "rgb(150, 150, 150)";
    colorOrig = "white"
    $("body").css("background-color", "white");
    $(".greetings1").css('background-color', 'rgb(150, 150, 150)')
  })
}
);

