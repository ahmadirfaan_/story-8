var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  });
}   

$("#buttonTheme1").click(function(){ 
    $("body").css("background-color","yellow");
    $("#greetings").css("background-color","orange")
});
$("#buttonTheme2").click(function(){ 
    $("body").css("background-color","white");
});